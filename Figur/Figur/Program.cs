﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figur
{
    class Program
    {
        static void PrintFigur(List<Figur> figur)
        {
            for (int i = 0; i < figur.Count; i++)
            {
                figur[i].Drow();
            }
        }
        static void Main(string[] args)
        {

            List<Figur> figur = new List<Figur>();
            figur.Add(new Triangle());
            figur.Add(new Rectangle(20, 8));
            figur.Add(new Arrow());
            figur.Add(new Square(5));
            PrintFigur(figur);
            Console.ReadLine();
        }
    }
}
