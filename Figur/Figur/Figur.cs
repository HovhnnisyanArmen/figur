﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figur
{
    class Figur
    {
        protected char[,] symbol { get; set; }

        protected virtual void CreateFigur(char[,] symbol)
        {

        }
        public virtual void Drow()
        {

            for (int i = 0; i < symbol.GetLength(0); i++)
            {
                for (int j = 0; j < symbol.GetLength(1); j++)
                {
                    Console.Write("{0}", symbol[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
