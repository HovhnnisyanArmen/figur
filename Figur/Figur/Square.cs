﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figur
{
    class Square:Figur
    {
        public Square(int size)
        {
            base.symbol = new char[size, size];
            CreateFigur(symbol);
        }
        public override void Drow()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(this.GetType().Name);
            base.Drow();
            Console.ResetColor();
        }
        protected override void CreateFigur(char[,] symbol)
        {
            for (int i = 0; i < symbol.GetLength(0); i++)
            {
                if (i == 0 || i == symbol.GetLength(0) - 1)
                {
                    for (int j = 0; j < symbol.GetLength(1); j++)
                    {
                        symbol[i, j] = '*';
                    }
                }
                symbol[i, symbol.GetLength(1) - 1] = '*';
                symbol[i, 0] = '*';
            }
        }
    }
}
