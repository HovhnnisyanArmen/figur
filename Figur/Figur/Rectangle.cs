﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figur
{
    class Rectangle:Figur
    {
        public Rectangle(int length, int width)
        {
            symbol = new char[width, length];
            CreateFigur(symbol);
        }
        protected override void CreateFigur(char[,] symbol)
        {
            for (int i = 0; i < symbol.GetLength(0); i++)
            {
                if (i == 0 || i == symbol.GetLength(0) - 1)
                {
                    for (int j = 0; j < symbol.GetLength(1); j++)
                    {
                        symbol[i, j] = '*';
                    }
                }
                symbol[i, symbol.GetLength(1) - 1] = '*';
                symbol[i, 0] = '*';
            }
        }

        public override void Drow()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(this.GetType().Name);
            base.Drow();
            Console.ResetColor();

        }

    }
}
