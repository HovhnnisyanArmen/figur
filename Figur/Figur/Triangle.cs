﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figur
{
    class Triangle:Figur
    {
        public Triangle()
        {
            base.symbol = new char[6, 11];
            CreateFigur(symbol);
        }
        public override void Drow()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(this.GetType().Name);
            base.Drow();
            Console.ResetColor();
        }
        protected override void CreateFigur(char[,] symbol)
        {

            symbol[0, 5] = '*';
            int x = 5;
            for (int i = 1; i < symbol.GetLength(0); i++)
            {
                symbol[i, x - i] = '*';
                symbol[i, x + i] = '*';
                if (i == 5)
                {
                    for (int j = 0; j < symbol.GetLength(1); j += 2)
                    {
                        symbol[i, j] = '*';
                    }
                }
            }
        }
    }
}
