﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figur
{
    class Arrow:Figur
    {
        public Arrow()
        {
            base.symbol = new char[5, 6];
            CreateFigur(symbol);
        }
        public override void Drow()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(this.GetType().Name);
            base.Drow();
            Console.ResetColor();

        }
        protected override void CreateFigur(char[,] symbol)
        {
            int x = 3;
            for (int i = 0; i < symbol.GetLength(0); i++)
            {
                if (i == 4)
                {
                    symbol[i, 3] = '*';
                    break;
                }
                symbol[i, x] = '*';
                x++;
                if (i == symbol.GetLength(0) / 2)
                {
                    for (int j = 0; j < symbol.GetLength(1); j++)
                    {
                        symbol[i, j] = '*';
                    }
                    x = 4;
                }
            }
        }
    }
}
